angular.module('healthiApp.controllers')
.controller('CtrlRestaurant',
  function($scope,$http,sharedProperties,$stateParams) {
  	 
    var serverPrefix = sharedProperties.getServerPrefix();
    $scope.establishment = $stateParams.establishment;
    $scope.address = $stateParams.address;
    $scope.inspections = [];
    $scope.yelpData = {};
    $scope.stats = {
      avgCriticals : 0,
      avgNoncriticals : 0,
      avgCriticalR : 0,
      avgNoncriticalR : 0
    };
    
    $scope.stateMap = {
      "tab.restaurantHome" : "Home",
      "tab.restaurantPest" : "Pest"
    };
    
    $scope.tabSuffix = $scope.stateMap[$scope.$viewHistory.currentView.stateName]

    $scope.map = {
        markers: [],
        defaults: {
            //tileLayer: "http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}",
            maxZoom: 14,
            zoomControl: false,
            //zoomControlPosition: 'bottomleft',
            path: {
                weight: 10,
                color: '#800000',
                opacity: 1
            }
        },
        center: { lat: 38.891121, lng: -77.041481, zoom: 12 }
    };

    var seriesCreation = function(chartConfig,data){
      var criticalSeriesObj = [];
      var noncriticalSeriesObj = [];
      _.each(data,function(item){
        var dateVal = Date.parse(item.date_of_inspection);
        var pointC = parseInt(item.critical_violations.total);
        var pointNC = parseInt(item.noncritical_violations.total);

        
        criticalSeriesObj.push([dateVal,pointC]);
        noncriticalSeriesObj.push([dateVal,pointNC]);
      });
      criticalSeriesObj = _.sortBy(criticalSeriesObj,function(item){ return item[0]; });
      noncriticalSeriesObj = _.sortBy(noncriticalSeriesObj,function(item){ return item[0]; });
      chartConfig.series.push({ name: 'Critical Violations', data: criticalSeriesObj });
      chartConfig.series.push({ name: 'Noncritical Violations', data: noncriticalSeriesObj });
   }

    var getYelpBizRating = function(yelp_id){
      $http.get(serverPrefix+ '/yelpBiz/'+yelp_id).     
      success(function (data, status, headers, config) {
        console.log(data);
        if (data.statusCode != 404){ $scope.yelpData = data; } 
      }).
      error(function (data, status, headers, config) {
        $scope.name = 'Error!'
      });
    };
    
    var getMarkerLatLng = function(data){
      if (typeof data.loc != 'undefined') {
        $scope.map.markers.push({lat: parseFloat(data.loc.lat), lng: parseFloat(data.loc.lon)});
        $scope.map.center = {lat: parseFloat(data.loc.lat), lng: parseFloat(data.loc.lon), zoom: 12 };
      }
    };

      $scope.chartConfig = {
          options: { 
            chart: { type: 'spline', backgroundColor: '#ffffff' },
            legend: { enabled: true },
            plotOptions: { spline: { marker: { enabled: false } } }, 
          },
            xAxis: { 
              type: 'datetime',title: { enabled: true /*text: 'Date of Inspection'*/ },
            },
            yAxis: { gridLineWidth: 0, minorGridLineWidth: 3,title: { text: '',enabled: true },min: 0},              
          title: { text: '' },
          series: [],
          credits: { enabled: false },
          loading: false
      };

      $scope.retrieveData = function(){
        $http.post(serverPrefix+ '/name/',  {establishment_name: $scope.establishment, address: $scope.address } ).     
        success(function (data, status, headers, config) {
          if (! data.length > 0) { console.log('No results for found'); }
          else { 
            _.each(data, function(item){ 
              $scope.inspections.push(item); 
              $scope.stats.avgCriticals += item.critical_violations.total;
              $scope.stats.avgNoncriticals += item.noncritical_violations.total;
              $scope.stats.avgCriticalR += item.critical_violations.r;
              $scope.stats.avgNoncriticalR += item.noncritical_violations.r;
            });
            $scope.stats.avgCriticals /= data.length;
            $scope.stats.avgNoncriticals /= data.length;
            $scope.stats.avgCriticalR /= data.length;
            $scope.stats.avgNoncriticalR /= data.length;

            seriesCreation($scope.chartConfig,data);
            //getYelpBizRating(data[0].yelp_id);
            getMarkerLatLng(data[0]);
          }
        }).
        error(function (data, status, headers, config) {
          $scope.name = 'Error!'
        });
      };
      $scope.retrieveData();

});