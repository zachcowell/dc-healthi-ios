angular.module('healthiApp.controllers')
.controller('CtrlObservations', 
  function($scope,$http,$stateParams,sharedProperties) {
    var serverPrefix = sharedProperties.getServerPrefix();
    $scope.obsArr = [];
    $scope.establishment = 'vvv';
    $scope.address = 'ddd';
    $scope.response_url = '#';
    $scope.retrieveData = function(){
      $http.get(serverPrefix+'/observations/'+$stateParams.reportId).     
      success(function (data, status, headers, config) {
        if (! data.length > 0) { console.log('No results for found'); }
        else { 
          _.each(data[0].observations, function(item){ 
            $scope.obsArr.push(item); 
          });
          $scope.establishment = data[0].establishment_name;
          $scope.address = data[0].address;
          $scope.response_url = data[0].response_url;
        }
      }).
      error(function (data, status, headers, config) {
        $scope.name = 'Error!'
      });
    };
    $scope.retrieveData();
});