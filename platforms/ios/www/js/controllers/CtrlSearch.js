angular.module('healthiApp.controllers')
.controller('CtrlSearch', 
  function($scope,$http,$stateParams,$filter,sharedProperties,$location) {
    $scope.restaurants = [];
    $scope.showMap = false;
    $scope.arrs = { autocomplete: [] }
    $scope.searchString = {text:''};
    $scope.searchTerm = typeof $stateParams.searchterm == 'undefined' ? sharedProperties.getProperty('lastSearch') : $stateParams.searchterm;
    var serverPrefix = sharedProperties.getServerPrefix();
    $scope.hello = function(){ window.open('http://www.google.com', '_blank'); }
    $scope.autocomplete = function() {
      $scope.arrs.autocomplete = [];
      if ($scope.searchString.text.length < 3) { return; }
          $http.post(serverPrefix+'/restaurantNames', {
              establishment_name: $scope.searchString.text
          }).then(function(res){
            var establishments = [];
            _.each(res.data, function(item){ 
              item = $filter('properCase')(item);
              establishments.push(item); 
            })
            $scope.arrs.autocomplete= _.sortBy(establishments,function(item) { return item; } );
          });
      };

    
    var getYelpBizRating = function(item,callback){
      $http.get(serverPrefix+'/yelpBiz/'+item._id.yelp_id).     
      success(function (data, status, headers, config) {
        callback(item,data);
      }).error(function (data, status, headers, config) { /*Do something*/ });
    };

    var restaurantsPopulation = function(item,yelpData){ 
      if (yelpData.statusCode != 404){ item.yelp = yelpData; } 
      $scope.restaurants.push(item); 
    };

    var retrieveData = function(){
      $http.post(serverPrefix+'/find/',  {establishment_name: $scope.searchTerm }).     
      success(function (data, status, headers, config) {
        if (! data.length > 0) { console.log('No results for found'); }
        else { 
          sharedProperties.setProperty('lastSearch',$scope.searchTerm);
          _.each(data, function(item){ restaurantsPopulation(item,restaurantsPopulation); }); 

        }
      }).error(function (data, status, headers, config) { /*Do something*/ });
    };
    retrieveData();
});