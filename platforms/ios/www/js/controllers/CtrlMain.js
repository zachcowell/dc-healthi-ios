angular.module('healthiApp.controllers')
.controller('CtrlMain', 
  function($scope,$http, sharedProperties) {

    var serverPrefix = sharedProperties.getServerPrefix();

    $scope.arrs = {
	    worstRestaurantsAvg: [],
	    worstRecentInspections: [],
	    pests: [],
      autocomplete: [],
      pestHighlights: []
    }
    
    var pestWords = sharedProperties.getProperty('pestWords');
    
    var populatePestHighlights = function(data){
      _.each(data,function(item){
        var highlight = {establishment_name: item.establishment_name, address:item.address, date_of_inspection: item.date_of_inspection, observations: [] }
        _.each(item.observations,function(observation){
          var obsItem = observation.observation;
          _.each(pestWords,function(word){  
            if (obsItem.indexOf(word) > -1) { highlight.observations.push(obsItem); }
          })
        });
      if (highlight.observations.length > 0) { $scope.arrs.pestHighlights.push(highlight); }
      });
    }
    
    $http.post(serverPrefix+'/keywordSearch/',  {keywords: pestWords}).     
    success(function (data, status, headers, config) {
      if (! data.length > 0) { console.log('No results for found'); }
      else { 
        $scope.pests = data; 
        populatePestHighlights($scope.pests);
      }      
    }).
    error(function (data, status, headers, config) {
      $scope.name = 'Error!'
    });
    

    $scope.fetch = function(endpoint,array,callback){
      if (typeof callback == 'undefined'){ var func = function (data, status, headers, config) {
          if (! data.length > 0) { console.log('No results for found'); }
          else { 
          	_.each(data, function(item){ 
          		array.push(item); }); 
          }
        } 
      }
      else { var func = callback; }

      $http.get(endpoint).success(func).error(function (data, status, headers, config) {});
    }

    $scope.fetch(serverPrefix + '/worst/restaurantsavg/',$scope.arrs.worstRestaurantsAvg);
    $scope.fetch(serverPrefix + '/worst/recentinspection/',$scope.arrs.worstRecentInspections);
});