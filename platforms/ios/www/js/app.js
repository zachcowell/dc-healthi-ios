// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
var healthi = angular.module('healthiApp', [
  'ionic', 
  'healthiApp.controllers',
  'healthiApp.filters',
  'leaflet-directive',
  'highcharts-ng'
  ])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('tab', {
      url: "/tab",
      abstract: true,
      templateUrl: "templates/tabs.html"
    })
    .state('tab.search', {
      url: "/search",
      views: {
        'tab-search' :{
          templateUrl: "templates/search.html",
          controller: "CtrlSearch"
        }
      }
    })
    .state('tab.searchterm', {
      url: "/search/:searchterm",
      views: {
        'tab-search' :{
          templateUrl: "templates/search.html",
          controller: "CtrlSearch"
        }
      }
    })
    .state('tab.more', {
      url: "/more",
      views: {
        'tab-more' :{
          templateUrl: "templates/more.html"
        }
      }
    })
    .state('tab.faq', {
      url: "/more/faq",
      views: {
        'tab-more' :{
          templateUrl: "templates/faq.html"
        }
      }
    })  
    .state('tab.contact', {
      url: "/more/contact",
      views: {
        'tab-more' :{
          templateUrl: "templates/contact.html"
        }
      }
    })     
    .state('tab.pestwatch', {
      url: "/pestwatch",
      views: {
        'tab-pestwatch' :{
          templateUrl: "templates/pest-watch.html",
          controller: "CtrlPest"
        }
      }
    })
    .state('tab.restaurant', {
      url: "/restaurant/:establishment/:address",
      views: {
        'tab-search' :{
          templateUrl: "templates/restaurant.html",
          controller: "CtrlRestaurant"
        }
      }
    })
    .state('tab.restaurantHome', {
      url: "/restauranthome/:establishment/:address",
      views: {
        'tab-nearby' :{
          templateUrl: "templates/restaurant.html",
          controller: "CtrlRestaurant"
        }
      }
    })
    .state('tab.restaurantPest', {
      url: "/restaurantpest/:establishment/:address",
      views: {
        'tab-pestwatch' :{
          templateUrl: "templates/restaurant.html",
          controller: "CtrlRestaurant"
        }
      }
    })
    .state('tab.observationsHome', {
      url: "/obsHome/:reportId",
      views: {
        'tab-nearby' :{
          templateUrl: "templates/observations.html",
          controller: "CtrlObservations"
        }
      }
    })
    .state('tab.observationsPest', {
      url: "/obsPest/:reportId",
      views: {
        'tab-pestwatch' :{
          templateUrl: "templates/observations.html",
          controller: "CtrlObservations"
        }
      }
    })
    .state('tab.observations', {
      url: "/obs/:reportId",
      views: {
        'tab-search' :{
          templateUrl: "templates/observations.html",
          controller: "CtrlObservations"
        }
      }
    })
    .state('tab.nearby', {
      url: "/nearby",
      views: {
        'tab-nearby' :{
          templateUrl: "templates/nearby.html",
          controller: "CtrlMain"
        }
      }
    });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/tab/nearby');
});

angular.module('healthiApp.filters', []).
  filter('startFrom', function() {
    return function(input, start) {
        start = +start; 
        return input.slice(start);
    }
}).
  filter('roundUp', function() {
    return function(input) { return Math.ceil(input); }
}).
  filter('unsafe', function($sce) {
    return function(val) {
        return $sce.trustAsHtml(val);
    }; }).
  filter('pestHighlighter', function(sharedProperties,$sce) {
    return function(input) { 
      var pestWords = sharedProperties.getProperty('pestWords');
      var newString = input.replace(/dropping|roach|mice|rodent|feces|mouse|flies|gnats/gi, function(matched){
          return "<strong>" + matched + "</strong>";
      });
      
      return newString;
    }
}).
  filter('properCase', function() {        
    return function(input) { 
      input = input.toLowerCase().replace(/\b[a-z]/g, function(letter) { return letter.toUpperCase(); });
      input = input.replace("'S","'s"); //Mcdonald'S becomes Mcdonald's
      return input;
    }
});

healthi.service('sharedProperties', function () {
    var obj = 
        { 
          serverPrefix: 'http://healthi.herokuapp.com',
          //serverPrefix: 'http://localhost:3000',
          lastSearch: 'am',
          pestWords: ['dropping','roach','mice','rodent','feces','mouse','flies','gnats']
        };

    return {
        getProperty: function (property) { return obj[property]; },
        setProperty: function(property,value) { obj[property] = value; },
        getServerPrefix: function(){ return this.getProperty('serverPrefix'); } //because im lazy
    };
});