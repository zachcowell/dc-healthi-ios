angular.module('healthiApp.controllers')
.controller('CtrlPest', 
  function($scope,$http,$stateParams,sharedProperties) {
    var serverPrefix = sharedProperties.getServerPrefix();
    $scope.pests = [];
    $scope.pageNum = 0;
    
    var pestWords = sharedProperties.getProperty('pestWords');
    
    var populatePestHighlights = function(data){
      $scope.pageNum++;
      _.each(data,function(item){
        var highlight = {establishment_name: item.establishment_name, address:item.address, date_of_inspection: item.date_of_inspection, observations: [] }
        _.each(item.observations,function(observation){
          var obsItem = observation.observation;
          _.each(pestWords,function(word){  
            if (obsItem.indexOf(word) > -1) { highlight.observations.push(obsItem); }
          })
        });
      if (highlight.observations.length > 0) { 
        $scope.pests.push(highlight); 
      }
      });
      $scope.$broadcast('scroll.infiniteScrollComplete');
    }

    $scope.getPestPage = function(){
      $http.post(serverPrefix+'/keywordSearch/',  {keywords: pestWords,pageNum:$scope.pageNum}).
      success(function (data, status, headers, config) {
        if (! data.length > 0) { console.log('No results for found'); }
        else { 
          populatePestHighlights(data);
        }      
      }).
      error(function (data, status, headers, config) {
        $scope.name = 'Error!'
      });
    }
    $scope.$on('stateChangeSuccess', function() {
      $scope.getPestPage();
    });
    

});