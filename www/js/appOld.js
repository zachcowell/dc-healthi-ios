
var healthi = angular.module('healthiApp', [
  'healthiApp.controllers',
  'ngRoute',
  'healthiApp.filters',
  'fsCordova',
  'ngAnimate',
  'leaflet-directive',
  'highcharts-ng'
]);

angular.module('healthiApp.controllers', []);

healthi.service('sharedProperties', function () {
    var obj = 
        { 
          serverPrefix: 'http://healthi.herokuapp.com',
          //serverPrefix: 'http://localhost:3000',
          lastSearch: 'am',
          pestWords: ['dropping','roach','mice','rodent','feces','mouse','flies','gnats']
        };

    return {
        getProperty: function (property) { return obj[property]; },
        setProperty: function(property,value) { obj[property] = value; },
        getServerPrefix: function(){ return this.getProperty('serverPrefix'); } //because im lazy
    };
});


angular.module('healthiApp.filters', []).
	filter('startFrom', function() {
		return function(input, start) {
		    start = +start; 
		    return input.slice(start);
		}
}).
  filter('roundUp', function() {
    return function(input) { return Math.ceil(input); }
}).
  filter('unsafe', function($sce) {
    return function(val) {
        return $sce.trustAsHtml(val);
    }; }).
  filter('pestHighlighter', function(sharedProperties,$sce) {
    return function(input) { 
      var pestWords = sharedProperties.getProperty('pestWords');
      var newString = input.replace(/dropping|roach|mice|rodent|feces|mouse|flies|gnats/gi, function(matched){
          return "<strong>" + matched + "</strong>";
      });
      
      return newString;
    }
}).
	filter('properCase', function() {        
		return function(input) { 
			input = input.toLowerCase().replace(/\b[a-z]/g, function(letter) { return letter.toUpperCase(); });
			input = input.replace("'S","'s"); //Mcdonald'S becomes Mcdonald's
			return input;
		}
});

healthi.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider
      .when('/restaurant/:establishment/:address', { templateUrl: 'partials/restaurant.html' })
      .when('/nearby/', { templateUrl: 'partials/nearby.html'})
      .when('/help/', { templateUrl: 'partials/help.html'})
      .when('/search/:searchterm', { templateUrl: 'partials/search.html'})
      .when('/search/', { templateUrl: 'partials/search.html'})
      .when('/obs/:reportId', { templateUrl: 'partials/observations.html'})
      .otherwise({ redirectTo: '/nearby/'});
  }]);